/*
main.cpp
Author : Smith Rajesh Dbritto <sd1327@live.mdx.ac.uk>
Created : 05/01/2022
Updated : 14/01/2022
*/

#include <iostream>
#include <vector>
#include <string>
#include <fstream>

	std::vector<std::string> users;
	std::vector<int> user_events;
	std::vector<int> user_status;
	std::vector<std::string> events_book
		{"Live Music : Press (1)",
	         "Film (2D/3D) : Press (2 or 3)",
	         "Stand-up Comedy : Press (4)"};

	std::vector<std::string> events{"Live Music", "Film", "Stand-up Comedy"};
	std::vector<int> bookings{0, 0, 0};
	std::vector<int> avail{ 10, 15, 10, 15, 10, 15};

long int booking_id = 0;

int main() {
    while (true){

        int option;
	std::cout<<"\n|----------------------------------------------------------| "<<std::endl;
        std::cout << " Event Booking System" << std::endl;
        std::cout << std::endl;
        std::cout << "1. Add a booking for an event"<< std::endl;
        std::cout << "2. Cancel/refund a booking"<< std::endl;
        std::cout << "3. List all the events"<< std::endl;
        std::cout << "4. List all the details and availability"<< std::endl;
        std::cout << "5. Load data from a file (All Events and Availability)"<< std::endl;
        std::cout << "6. Save data to a file (All Events and Availability)"<< std::endl;
        std::cout << "7. Exit"<< std::endl;
        std::cout << std::endl;
	std::cout<<"\n|----------------------------------------------------------| "<<std::endl;        
        std::cout << "Please enter a numeric value of the options listed above: " << std::endl;
        std::cin >> option;



        if(option==1){
            std::cout << std::endl;
            std::cout << "Please enter your choice of venue:" << std::endl;
            std::string venue;
            std::cin >> venue;
            users.push_back(venue);

            std::cout << std::endl;
            std::cout << "Please select the Event that you want to book" << std::endl;
            std::cout << std::endl;

            for (const auto& i: events_book){
                std::cout << i << '\n';
            }

            int event_index;
            std::cin >> event_index;
            std::cout << std::endl;

            if (bookings[event_index -1] <= avail[event_index - 1]){
                std::cout << "Please Confirm Details" << std::endl;

                std::string sub_event = "2D";
                int event_sub_ind = 0;

                if (event_index%2==0){
                    sub_event = "3D";
                    event_sub_ind = (event_index/2) - 1;
                }
                else{
                    event_sub_ind = (event_index/2);
                }
                std::cout << "Venue Capacity: " << venue << ", " << "Event: " << events[event_sub_ind] << " " << sub_event << ", " << std::endl;
                std::cout << std::endl;
            }
            else {
                std::cout << "Event Not Available" << std::endl;
                continue;
            }

            std::cout << "Please Confirm Booking : (Yes : 1  or  No : 0)" << std::endl;
            int confirm;
            std::cin >> confirm;

            if (confirm){
                std::cout << "Your booking has been confirmed!, Your booking ID is :  " << booking_id << std::endl;
		std::cout<<"\n|-----------------------------------------| "<<std::endl;
                std::cout << std::endl;
                booking_id += 1;
                avail[event_index - 1] +=1;
                users.push_back(venue);
                user_events.push_back(event_index);
                user_status.push_back(1);
            }
        }

        else if (option == 2){
            std::cout << std::endl;
            std::cout << "Please Enter Booking ID for Cancellation" << std::endl;


            int book_id;
            std::cin >> book_id;
            std::cout << std::endl;

            int user_stat = user_status[book_id];

            if (!user_stat){
                std::cout << "Booking Already Cancelled!" << std::endl;
                continue;
            }

            int event_index = user_events[book_id];

            std::cout << "Please Confirm Details" << std::endl;

            std::string sub_event = "2D";
            int event_sub_ind = 0;

            if (event_index%2==0){
                sub_event = "3D";
                event_sub_ind = (event_index/2) - 1;
            }
            else{
                event_sub_ind = (event_index/2);
            }
            std::string venue = users[booking_id];
            std::cout << "Venue Capacity: " << venue << ", " << "Event: " << events[event_sub_ind] << " " << sub_event << ", " << std::endl;
            std::cout << std::endl;

            std::cout << std::endl;
            std::cout << "Please Confirm Cancellation: (Yes : 1  or  No : 0)" << std::endl;
            int confirm;
            std::cin >> confirm;

            if (confirm){
                std::cout << "Booking Cancelled!" << std::endl;
                bookings[event_index-1] -= 1;
                user_status[book_id] = 0;
                std::cout << std::endl;
            }
            else{
                continue;
            }

        }
        
        else if (option == 3)
		{	
		  	  std::cout<<"\n All the events that are available are: \n"<<std::endl;
		  	  std::cout<<"\n 1. Live Music "<<std::endl;
		  	  std::cout<<"\n 2. Stand-Up Comedy "<<std::endl;
		  	  std::cout<<"\n 3. Film "<<std::endl;
		  	  std::cout<<"\n|-----------------------------------------| "<<std::endl;
			  
		}    
 	
 	else if (option == 4)
 		{
 			std::cout<<"\n Listing all the details for the event: "<<std::endl;
 		
 		}
 		
 	else if (option == 5)
 		{
 			std::cout<<"\n Loading file..."<<std::endl;
 			     
        	}
        	
        else if (option == 6)
        	{
			std::cout<<"\n Saving to file..."<<std::endl;
			
        	}
        
        else if (option == 7)
        	{
            		std::cout << std::endl;
            		std::cout << "Do you want to Exit ?" << std::endl;
            		std::cout << "Yes : 1  or  No : 0" << std::endl;

            int exit;
            std::cin >> exit;
            if (exit){
                break;
            }
        }

    }
 return 0;
}